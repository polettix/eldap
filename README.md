# NAME

eldap - Query an LDAP directory

# VERSION

The version can be retrieved with option `--version`:

    $ eldap --version

# USAGE

    eldap [--help] [--man] [--usage] [--version]

    eldap [--base|-b string]
          [--host|-h string]
          [--output|-o filename]
          [--pagesize|--page-size|-P integer]
          [--pass|--password|-p string]
          [--user|--username|-u string]
          [--wait|--wait-time|-W integer]
          <search-filter>

# EXAMPLES

    ./eldap -h ldap://www.zflexldap.com \
       -b 'ou=developers,dc=zflexsoftware,dc=com'   
       -u 'cn=ro_admin,ou=sysadmins,dc=zflexsoftware,dc=com' \
       -p zflexpass \
       -P 1000 \
       -o testdata.json \
       '(objectclass=organizationalPerson)'

# DESCRIPTION

The goal of this little program is easily explained: send a request
towards a LDAP directory and save the resulting entries as an array of
objects, encoded in JSON.

Everything is passed as a command-line option, possibly specified
through environment variables. The only exception is the query/filter
string, which is passed without an explicit option name (i.e. the first
"unnamed" command line argument).

# OPTIONS

- **--base|-b string**

    the base for doing the LDAP search. Can be set through environment
    variable `ELDAP_BASE`.

- **--help**

    print out some help and exit.

- **--host|-h string**

    the host of connection string, e.g. `ldaps://whatever.com` would be
    fine. Can be set through environment variable `ELDAP_HOST`.

- **--man**

    show the manual page for eldap.

- **--output|-o path**

    the path for the JSON-encoded output. Defaults to standard output,
    corresponding to the conventional file name `-`.

- **--pagesize|--page-size|-p integer**

    the size of each page to ask. Paging allows overcoming the server's
    limits in some LDAP technologies (notably Active Directory). Can be set
    through environment variable `ELDAP_PAGESIZE`, defaults to 100 records
    per page.

- **--pass|--password|-p string**

    the password for the login (_binding_) to the LDAP directory. Can be
    set through environment variable `ELDAP_PASS`.

- **--user|--username|-u string**

    the _username_, or namely the distinguished name to use for the
    login/binding. Bear with me please.

    Can be set through environment variable `ELDAP_USER`

- **--usage**

    show usage instructions.

- **--version**

    show version.

- **--wait|--wait-time|-W integer**

    time in seconds to wait between the retrieval of two consecutive pages,
    allows throttling the requests to the server and avoid clogging it. Can
    be set through environment variable `ELDAP_WAIT`. Defaults to 1 second.

# CONFIGURATION

All configuraiton happens through the comand line parameters and
associated environment variables, when available. See ["OPTIONS"](#options).

# DEPENDENCIES

    requires 'Net::LDAP';
    requires 'URI';
    requires 'IO::Socket::SSL';
    requires 'Cpanel::JSON::XS';
    requires 'Try::Catch';

# BUGS AND LIMITATIONS

Please report any bugs or feature requests to the ["AUTHOR"](#author).

# AUTHOR

Flavio Poletti

# LICENSE AND COPYRIGHT

Copyright 2021 by Flavio Poletti (flavio@polettix.it).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

or look for file `LICENSE` in this project's root directory.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
